# Self-Managed VM Role

## Preamble
This is one approach for a self-managed role, and remains a work-in-progress.  
  
## Approach(es)
The idea behind a role for provisioning self-managed VMs is to provide the base environment a Linux system, as well as to provide the base tools for system administrators to edit files (config, etc).  
  
## Using the role in a playbook
```yaml
# some_playbook.yml

- name: My Playbook for a DUL project
  hosts:
    - self_managed
    - my_server

  roles:
    - { role: self_managed, tags: [roles, self_managed] }
```

## Tasks
These are the current tasks available in this role:

### main.yml
Installs the base packages needed to compile other packages, and packages for editing (eg. emacs).  

### create_user.yml
Create a user specified by the parameter, `target_user`.

### create_group.yml
Create a group specified by the parameter, `target_group`.

### install_solr.yml
(Work-in-progress) Install a version of Solr.

### 