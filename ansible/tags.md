# Ansible Playbook Tag Dictionary

- Tagging in a role in a playbook

```yaml
roles:
  - { role: shibboleth, tags: shibboleth }
```

- Tagging inside a role

```yaml
- name: This is a task
  command: /bin/true
  tags: role_task
  # role = the name of the role
  # and task is something else, like "config"
```
