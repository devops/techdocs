# Dell Precision 5540 + Fedora 31

## Avoid use of NVidia graphics adapter

NVidia does not collaborate with the free and open source software
communities to create CPU-side drivers for their graphics
adapters. Instead, the community develops the
[Nouveau](https://nouveau.freedesktop.org/wiki/) driver based on
reverse-engineering work.

The Nouveau version included with Fedora 31 and Linux 5.3 does not
support the NVidia Quadro T100 Mobile (TU117GLM) in the
Precision 5540. This results in Linux reporting stack traces from the
driver and excessive power usage. Fortunately, the Precision 5540 also
has an Intel graphics adapter supported by free drivers, so we can
avoid these problems by avoiding the use of the Nouveau driver:

* Configure the Linux kernel module loader to blacklist the Nouveau
  driver and avoid using it for kernel modesetting by creating
  `/etc/modprobe.d/disable-nouveau.conf` with the following contents:
  
  ```
  blacklist nouveau
  options nouveau modeset=0
  ```

* Rebuild the initramfs to include these new modprobe settings by
  running the following as root:
  
  ```
  dracut -f
  ```

* Modify the Linux command line to avoid loading Nouveau in early boot
  by adding `rdblacklist=nouveau` to `GRUB_CMDLINE_LINUX` in
  `/etc/default/grub`.

* Rebuild the grub configuration by running the following as root:
  
  ```
  grub2-mkconfig -o /boot/grub2/grub.cfg
  ```

* Reboot

## Fix: Docker container runtime error

```
OCI runtime create failed: container_linux.go:346: starting container process 
caused "process_linux.go:297: applying cgroup configuration for process caused 
\"open /sys/fs/cgroup/docker/cpuset.cpus.effective: no such file or directory\"": 
unknown
```

https://github.com/docker/for-linux/issues/665

Sudo edit `/etc/default/grub` add to `GRUB_CMDLINE_LINUX`:

    systemd.unified_cgroup_hierarchy=0
    
Run

    $ sudo grub2-mkconfig -o /boot/grub2/grub.cfg
    
Reboot
