# Docker Recommendations: Initializing a new project

- Make sure your Docker environment is up to date.  Check for updates in the Docker Desktop client.  
It’s also good to stay up to date on your version of source-to-image, although it changes more slowly.

- Make sure there are no running containers.  `docker ps` will give that information.  If you want 
to stop all running containers, try: `docker ps -q | xargs docker stop`.

- If you haven’t restarted Docker in a while -- and you didn't need to update -- it wouldn't hurt to do so.

- Prune your docker environment.  Use `docker system prune` and add the `--volumes` option to also remove volumes.

Also:

- In Docker Desktop, check the Resources configuration.  You may want to try different settings if you are having performance issues.

