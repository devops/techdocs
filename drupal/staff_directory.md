# Staff Directory Project

## Preamble
This document describes, in some detail, the DUL-built "Drupal" module that currently powers the display of the Libraries' Staff Directory, located here:  
https://library.duke.edu/about/directory  
  
## Module History
The module `(dul_staff)` was created as a port of the ITS legacy Django web application used to display the Libraries' Staff Directory under our old Cascade web server.