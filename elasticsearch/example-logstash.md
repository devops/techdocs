# Logstash To Elasticsearch Example

In the example below, syslogs will be forwarded to the Elasticsearch instance hosted on 
`event-logging.lib.duke.edu`.  
  
```
###################################################
# FILE: /etc/logstash/conf.d/file-tracker-dev.conf
#
###################################################

# read /var/log/messages
input {
  file {
    path => [
      "/var/log/messages"
    ]
    type => "syslog"
  } 
}

# your basic examples will not require additional filters

# forward the log events to 'event-logging'
output {
  elasticsearch {
    hosts => ["event-logging:9200"]
    index => "file-tracker-dev-%{+YYYY.MM.dd}"
  }
}
```
