# FTP Depot: Rubenstein's Large File Sharing Project

[[_TOC_]]

## Background
Rubenstein Library staff approached Core Services about creating a service for 
sharing large files, primarily due to size limitations of Duke's Box platform.  

## Storage
Files reside on the `F:` drive, under a `Trucks/RubLgFileDlvry` folder created by Desktop Support and DevOps, and is accessible 
in Windows by Rubenstein Library staff. This has provided an easy, efficient method of file access.  

## The Problem We Wanted To Solve
**Primary**  
Providing non-Duke guests (vendors) with secure, hassle-free access to files shared by Rubenstein Library staff (located on the `F:` drive).  
  
**Secondary**  
Reduce dependency on OIT for system configuration  

## Service Accounts
### Accounts for "Retrieving" Guests
* rubguest01
* rubguest02
* rubguest03  
  
These NetIDs belong to the following Grouper group:  
  
`urn:mace:duke.edu:library:posix:rubenstein-sftp`  

### Accounts for "Depositing" Guests
* rubguest04  
  
Non-Duke "Guests" can FTP into **ftpdepot.lib.duke.edu** with this NetID to deposit large files for sharing with staff from either 
Rubenstein or Digital Collections/Curations Services.

## Service Account Passwords
Passwords can be inspected on **automation-01** by:
```bash
$ cd antsy2
$ ansible-vault view vars/accounts/<rubguest-file>
```
..where `<rubguest-file>` is one of "rubguest1.yml", "rubguest2.yml", or "rubguest3.yml"

## Workflow Descriptions
### Rubenstein Staff
Members of Rubenstein deposit (or share) files in designated folder on `F:` drive, and notify vendors when files available.  
When files are ready for sharing, Rubenstein will also share login credentials (rubguest01, rubguest02, rubguest03).
### Non-Duke Guests
Guests use FTP client to open connection to **ftpdepot.lib.duke.edu** using credentials provided by Rubenstein.  
Upon successful connection, guests are placed into appropriate folder and can download files as needed.
#### Technical Details
`sshd_config` is configured to "chroot jail" users into appropriate directory.

## Technical Info on Shared Directory (Trucks)

### autofs
```bash
LIB-RUBFTP -fstype=cifs,username=RubFTP-lib,password="<redacted>",domain=WIN,mfsymlinks,dir_mode=0755,file_mode=0777,gid=rubenstein-sftp \
://oit-nas-fe10-node3.oit.duke.edu/lib-fs/Vol1/Trucks/RubLgFileDlvry
```

### sshd_config
```bash
Match group rubenstein-sftp
  ChrootDirectory /nas/LIB-RUBFTP
  ForceCommand internal-sftp
```

## Next Steps
### F: Drive Migration
Desktop Support started a process of migrating data off of the `F:` drive prior to Chris Burton's departure.   
DevOps will work with Desktop Support to continue this effort such that workflow disruptions are minimal.
