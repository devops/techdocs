# Update a remote from GitHub to GitLab

Check the current URL of the `origin` remote in the project:

```sh
[dc@localhost rdr]$ git remote get-url origin
git@github.com:duke-libraries/rdr.git
```

Get the *SSH* URL for the GitLab repository by going to the project home page
and clicking the `Clone` dropdown.

Change the URL of the `origin` remote to the new URL:

```sh
[dc@localhost rdr]$ git remote set-url origin git@gitlab.oit.duke.edu:ddr/rdr.git
[dc@localhost rdr]$ git remote get-url origin
git@gitlab.oit.duke.edu:ddr/rdr.git
```