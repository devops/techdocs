# Create an alias for a merge request

With this alias I can push my branch to the server and create a merge request with one command:

    $ git mr origin my-branch

Here's how.

The long form of the request is:

    $ git push -o merge_request.create origin my-branch

This uses git [push options](https://docs.gitlab.com/ee/user/project/push_options.html) 
recognized by the GitLab server 
and will create a merge request on the default branch.  

We will create an "alias" for the "push -o ..." part as follows.

    $ git config --global alias.mr "push -o merge_request.create"

If you check your [global config](https://www.git-scm.com/book/en/v2/Customizing-Git-Git-Configuration) 
you should see it listed:

    $ git config --global --list
    ...
    alias.mr=push -o merge_request.create
    ...




