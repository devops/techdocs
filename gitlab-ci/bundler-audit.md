# Bundler audit

## Requiring bundler-audit to pass in CI

In the .gitlab-ci.yml file, you might currently see something like this at the top:

```yaml
include:
  - project: devops/ci-config
    file:
      - /templates/bundler-audit.gitlab-ci.yml
      - /templates/container-flexible.gitlab-ci.yml
      - /templates/environments.gitlab-ci.yml
      - /templates/tag-release-version.gitlab-ci.yml
    ref: main
  - local: /version.yml
```

The bundler-audit template allows the task to fail.

To require bundler-audit to pass, replace

    - /templates/bundler-audit.gitlab-ci.yml

with

    - /templates/audit-jobs.gitlab-ci.yml

and add at the top level of `.gitlab-ci.yml`

```yaml
bundler-audit:
  allow_failure: false
```

Also, if the `ref` key is present as shown above, make sure it is set to `main` (you don't need to add it if missing).
