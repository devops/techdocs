# OpenShift Environment for DUL-ITS
[[_TOC_]]
## Projects Currently Using OKD

## Production Apps & Services

| App/Service | Hostname | OKD Cluster | Namespace | Helm-managed | GL Webhook Managed |
| ----------- | -------- | ----------- | --------- | :----: | :----: |
| Discovery | find.library.duke.edu | OKD3 | duo-catalog | Y |   |
| Huginn | huginn.library.duke.edu | OKD3 | dul-huginn |   |   |
| Payments | payments.library.duke.edu | OKD4 Prod | dul-payments-prod | Y |   |
| Quicksearch | quicksearch.library.duke.edu | OKD3 | dul-dst |   | Y |
| Spacefinder | spacefinder.lib.duke.edu | OKD3 | dul-spacefinder | Y |   |
| Staff Directory | directory.library.duke.edu | OKD3 | dul-directory-prod | Y |   |

## Building Custom Images / Pushing to OKD Project

These commands were used to push a customized Perl Dancer2 image to an OKD imagestream.  
  
```sh
$ cd /path/to/project
$ oc login ... (obtain login command from OKD console)
$ [sudo] docker login -u openshift -p $(oc whoami -t) registry.cloud.duke.edu
$ [sudo] docker build -t <mytag> .
$ [sudo] docker tag <mytag> registry.cloud.duke.edu/$(oc project -q)/<my-app-tag>:latest
$ [sudo] docker push registry.cloud.duke.edu/$(oc project -q)/<my-app-tag>:latest
```

### OKD Image Registries
okd3 - `registry.cloud.duke.edu`  
okd4-dev - `registry.apps.dev.okd4.fitz.cloud.duke.edu`  
okd4 - `registry.apps.prod.okd4.fitz.cloud.duke.edu`  

### Using DUL Domains on Duke's OKD

### OKD3
Simple point a CNAME to `os-node-lb-fitz.oit.duke.edu`. Routes can then be created using the hostname.

### OKD4
Coming soon.
  
Then, exposing a service -- such as:  
`oc expose svc/<my-app> --hostname=<my-dul-hostname>`
  
...will allow us to use http://my-dul-hostname in a web browser (or https://my-dul-hostname).
  
### Integrating NFS Volumes
Nate Childers (OIT) can configure a PersistentVolume for our NFS shares at our request.  

#### `subpath` Option
It's helpful of the `subpath` option has been enabled for a particular share. This can be done by submitting a 
ServiceNow ticket to Scott Steagall (Systems-Vmware Storage and Backups-OIT)

#### Exporting NFS Shares to Duke's OpenShift Cluster
Add `10.138.5.0/24` to the list of target servers.
  
### Persistent Volume Claim YAML Example
**volume-lib-asset-share** is a PersistentVolume configured by Nate Childers, using our oit-nas-fe11.oit.duke.edu:/LIB-ASSETS-SHARE NFS share.  
  
**Adding a PersistentVolumeClaim** is as easy as this:
```yaml
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: volume-lib-assets-share
spec:
  accessModes:
  - ReadWriteMany
  resources:
    requests:
      storage: 1Ti
  storageClassName: standard
  volumeName: volume-dul-sandbox-lib-assets-share
status: {}
```
It's best to create a template file and `oc apply -f <your-template>.yml`
