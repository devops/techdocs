# DDR: Cache Management

Both ddr-admin and ddr-public implement caches served by Memcached.

## Clearing the Cache

### ddr-public

Go to the `dul-ddr-public-prod` project in OKD, and navigate to the [`rails-cache` Deployment](https://console.apps.prod.okd4.fitz.cloud.duke.edu/k8s/ns/dul-ddr-public-prod/deployments/rails-cache).  Using the `Actions` drop-down menu to the right, select `Restart rollout`.  
You should see a new pod spinning up and replacing the previous one.

### ddr-admin 

One way to clear the cache is simply to restart the `cache` service.

Make sure you are connected to the VPN, then SSH to the server:

    $ ssh ddr-admin.lib.duke.edu

(On Windows, use a GUI terminal application like PuTTY.)

You will be prompted for your NetID password and multi-factor authentication.

Change to the project directory:

    [netid@ddr-admin-prod-11]$ cd /opt/docker-compose/projects/ddr-admin

Restart the cache service:

    [netid@ddr-admin-prod-11]$ sudo docker-compose restart cache

Logout of the server.