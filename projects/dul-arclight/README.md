# dul-arclight

Code repo: https://gitlab.oit.duke.edu/dul-its/dul-arclight

Playbook: https://gitlab.oit.duke.edu/devops/antsy/-/blob/main/arclight.yml

## Summary

dul-arclight is a containerized application running on [Docker Swarm](https://docs.docker.com/engine/swarm/).

The initial deployment was orchestrated with docker-compose, *which is still in
use in local development environments* (i.e., via the wrapper scripts `dev.sh`, etc.).

Since Docker swarm mode does not provide a single command like `docker-compose`,
a wrapper script `dul-arclight` has been provided to ease the usage of `exec` and
`logs`.  That is, we have aimed to make `dul-arclight exec` and `dul-arclight logs`
function like their `docker-compose` counterparts.  

Access a shell in the `app` service:

    $ dul-arclight shell app
    
Tail the logs of the `app` service:

    $ dul-arclight tail app

It should be understood, 
however, that under the covers swarm mode is a quite different way of running Docker.
Most important for our purposes at this time, in
swarm mode, a "stack" of services (as can be defined in a compose file) can be
re-deployed (i.e., updated) without restarting, which minimizes interruptions.

## Systemd Integration

A systemd unit template `/etc/systemd/system/docker-stack@.service` defines the
basic control configuration and an instance unit `docker-stack@dul-arclight.service`
is enabled. 

To manually re-deploy the running stack: 

    $ sudo systemctl reload docker-stack@dul-arclight.service
    
(You'll notice this is the command used in the CI/CD config.)    
    
If there is a need to bring the stack 
down and up again, I would advise against using `restart`; instead `stop`, check for 
running docker processes, and then `start`:

    $ sudo systemctl stop docker-stack@dul-arclight.service
    $ docker ps
    [no running containers]
    $ sudo systemctl start docker-stack@dul-arclight.service

## Logs

As mentioned in the Summary section above, the command `dul-arclight logs` will 
provide access to the Docker logs for *one service* -- e.g., `app`, `web`, etc. --
but does not provide a single log stream for the stack in the same way as 
`docker-compose logs`.

The consolidated logs from the "stack" are available in a different form with the command:

    $ journalctl -u docker-stack@dul-arclight.service [OPTIONS]
    
Check `man journalctl` for command summary and options.    
