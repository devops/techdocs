# TRLN-Discovery 

The Blacklight application behind the Library catalog (find.library.duke.edu).

## Rake Tasks

To run rake tasks, login as the `rails` user, change to the project directory,
and execute the task:

    $ sudo -u rails -i
    $ cd /opt/trln-discovery/root
    $ bundle exec rake ...