# Rails Deployment

This document is intended to describe the parameters of container image builds 
and deployment of Rails applications in the Library.

## Supported Software Versions

| Ruby | Rails | Bundler | Node.js | Yarn |
|------|-------|---------|---------|------|
| 2.6  | 5.2   | 2.0.2   | 12 |      |
| 2.7  | 6.0   | 2.1.4   | 12 |      |

### Notes

Specify the Ruby version in `Gemfile`:

    ruby '~> 2.6.6'

Note that you may also have a `.ruby-version` file in your project, but
that file is advisory only -- e.g., RVM uses it to switch Rubies.  
The Gemfile Ruby spec is bunding on the bundle.

The Rails gem spec should always allow for easy security updates via `bundle update rails`
without having to change `Gemfile` itself:

    gem 'rails', '~> 5.2.4'

Please install the exact Bundler 2.x version and remove others (1.x version is OK to leave installed).
This helps to avoid the annoying warning:

    Warning: the running version of Bundler (2.0.2) is 
    older than the version that created the lockfile (2.1.1). 
    We suggest you upgrade to the latest version of Bundler 
    by running `gem install bundler`.

List the Bundler versions installed

    $ gem list bundler

Remove other 2.x versions:

    $ gem uninstall bundler:2.1.1

Install the correct Bundler version:

    $ gem install bundler:2.0.2

Update `Gemfile.lock` (without installing gems):

Manually delete the `BUNDLED WITH` section:

    BUNDLED WITH
       2.1.1

Then regenerate it:
 
    $ bundle lock --update

(Alternatively, you could manually edit the version number under BUNDLED WITH.)

## Container Image Building Process

### Base Image

Debian Buster from official Docker base image.

#### Software

    - curl 
	- gosu (sudo-like wrapper for stepping down from root)
	- less 
    - locales 
	- nano (editor)
	- ncat ("netcat")
	- wait-for-it (wait for TCP services to begin listening)

#### Environment

| Variable | Default | Comments |
|----------|---------|----------|
| LANG     | en_US.UTF-8 | Locale setting |
| LANGUAGE | en_US:en |         |
| LC_ALL   | en_US.UTF-8 |      |
| TZ       | US/Eastern | Local time zone |

### Ruby

#### Software 

Added to the base image:

    - Software build tools (gcc, make, etc.)
    - Ruby build dependencies (development libraries, etc.)
    - Postgres support (client + development library)
    - Archive utils (zip/unzip, bzip2, xz-utils)
    - JavaScript support (Node.js, NPM, Yarn)

#### Environment

Environment variables added to base image (above).

| Variable | Default | Comments |
|----------|---------|----------|
| RUBY_HOME | /opt/ruby | Technically an ARG, used for installing Ruby, setting PATH. |
| APP_ROOT | /opt/app-root | Directory where app in installed (`Rails.root`) |
| APP_USER_UID | 1001 | User id of the app user |
| APP_USER_NAME | app-user | User name of the app user |
| GEM_HOME | $RUBY_HOME/bundle | Default location where gems are installed |
| PATH | $GEM_HOME/bin:$RUBY_HOME/bin:$PATH | Put gem and Ruby exectuables at head of PATH |
| BUNDLER_VERSION | (see matrix) | Bundler version is set by builder script |
| BUNDLE_APP_CONFIG | $GEM_HOME | Tells Bundler to look here for config, not in app (working) directory |
| BUNDLE_DISABLE_PLATFORM_WARNINGS | true | Disable warnings during install when a dependency is unused on current platform. |
| BUNDLE_DISABLE_VERSION_CHECK | true | Stop Bundler from checking for newer Bundler version. |
| RAILS_ENV | development | Default Rails environment |
| RAILS_LOG_LEVEL | info | Non-standard variable to control Rails logging level |
| RAILS_LOG_TO_STDOUT | 1 (enabled) |  We log to stdout, not file, so logs flow through Docker  -- i.e., use `docker logs` or `docker-compose logs`, etc. As deployed to server logging is routed from Docker to the systemd journal. |
| RAILS_PORT | 3000 | TCP port that is "exposed" in the Rails app container. Exposed posrts are accessible to other services in the same stack (via docker-compose or swarm mode), but  must be "published" (possibly on a different port number) to be accessible outside the Docker private network -- including on the host machine. Usually this port is accessed through a reverse proxy. *We should NOT rely on this specific value in code.* |
| RAILS_SERVE_STATIC_FILES | 1 (enabled) | Tells Rails to serve static assets and pages. Since puma is a production-ready web server, this is usually enabled; in the past it was often disabled and Apache served static files directly. |
