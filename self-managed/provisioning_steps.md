# Provisioning Self-Managed VM for Playbook Usage 

Provisioning a self-managed playbook for use with an Ansible playbook.

## Steps

**1.a. Add the new host to an inventory file** (`development`, `staging`, or `production`).

**1.b. Optionally customize the initial VM request parameters** in a `host_vars` file, e.g.:

```yaml
vm_options:
  cpu: 4
  ram: 8
```

**2.a. PLAYBOOK REQUEST PROCESS.**  Run the `vm-request.yml` playbook, e.g.:

    $ ansible-playbook -i INVENTORY -l HOSTNAME vm-request.yml
    
Notes: 
- The vm-request process is idempotent -- i.e., it will not create duplicate requests.  
- Future runs will update the VM if anything has changed such as CPU or RAM. (Changes in storage are not currently supported.)

**2.b. WEB FORM REQUEST PROCESS.** Order a new VM from [Clockworks](https://clockworks.oit.duke.edu). You'll need to set the following:

    Fund code        = [See your manager/department head for this]
    Syadmin Option   = Self Administration (Customer responsible for OS install)
    Container        = Lib-DevOps
    Operating System = Linux
    OS Version       = CentOS 7 (64-bit)
    Network Type     = Select Network -> OIT-DCLB-FITZEAST-LIBRARY-01 (152.3.9.192/26)

**3. Run the bootstrap process in Ansible:**

    $ bin/bootstrap.sh INVENTORY HOSTNAME

The bootstrap process will wait for the new VM to boot and SSH to become available,
then run the common playbook. After successful bootstrapping, the `unbootstrap.yml` playbook
will run to remove the bootstrap user. (Note that the task that removes the user may have to retry once
or twice to complete.)