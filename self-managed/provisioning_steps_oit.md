# Provisioning OIT Self-Managed VM for Playbook Usage #
Provisioning an OIT self-managed server/VM for use with an Ansible playbook.  
  
## Steps (to-date) ##
*Please note these steps are **subject to change***.  
  
1. Order VM through OIT's Clockworks
2. Complete "Acceptance" Process on new VM
3. Copy `libautomation` user's public key from **libautomation-01** (copy contents of /home/libautomation/.ssh/id_rsa.pub)
4. Adjust `/etc/sudoers` on new VM
  
### 1. Order VM Through OIT's Clockworks ###
Order a new "self-managed" VM from **clockworks.oit.duke.edu**.  You'll need to set the following:
- **Syadmin Option** = "Self Managed"
- **Hostname** = _anysubdomainof_.lib.duke.edu, or _anysubdomainof_.library.duke.edu
- **Fund code** -- see your manager/department head for this.
- **Operating System** = "Linux"
- **OS Version** = any of the "Pre-Installed" options

I typically use "Public Network" and "No Backups Needed", but feel free to adjust these to your liking.

### 2. Complete "Acceptance" Process on new VM ###
It typically takes 5-10 minutes for a new self-managed VM to be available at [OIT's VMWare Site](https://vmware.oit.duke.edu).  
  
Upon availability, you'll need to open a remote console, and complete the initial steps which may or may not include:
- Accepting End-User Agreement (True for RHEL)
- Creating initial non-root user (True for RHEL and Ubuntu)

### 3. Copy Public SSH Key (from libautomation-01) ###
See one of the DevOps to obtain `libautomation`'s public key from **lib-automation-01.oit.duke.edu**.  
  
1. Create the .ssh directory on the newly provisioned server.
2. Set the permissions for the above directory to 700 `(chmod 0700 .ssh)`
3. Copy the public key to .ssh/authorized_keys

  
### 4. Adjust `/etc/sudoers` File on new VM ###
Create a file named "perkins" under /etc/sudoers.d/, containing the following content:  
`<username> ALL=(ALL)   NOPASSWD: ALL`

This will allow a remotely run Ansible playbook to run sudo commands on the new VM without having to enter a password.  
For example:  
`libautomation  ALL=(ALL)   NOPASSWD: ALL`