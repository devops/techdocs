# Code Project Version Practices: Proposal

- **Follow [Semantic versioning](https://semver.org) practices**

- **Store the version number as the sole content of a text file named `VERSION`
in the root of the project.**  

Example:

    1.0.0

This practice allows the version file to be managed consistently
across projects, independently of language or framework.  

As a *counterexample*, Ruby projects by convention typically store the version 
number in a path like `lib/my_project/version.rb`, in a constant 
`MyProject::VERSION`.

