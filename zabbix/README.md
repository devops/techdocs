# Zabbix Documentation

Zabbix is open source monitoring software.

The [product documentation](https://www.zabbix.com/documentation/4.2/manual) is quite good.
Be sure to select the correct version (especially if searching the web).

The pages here supplement the product documentation with procedures and configuration
specific to our installation and work environment.
