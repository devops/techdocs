# Zabbix User Media Configuration

Zabbix users have no media enabled by default -- which means they receive no messages by default!

For each media type added, problem severity and active time windows can be configured.  The default
includes all problem severity levels and all times.

## Email

Sends email to an address.

### Slack

Sends direct messages to your Slack account in the [DST organization](https://dukelibrepo.slack.com).
Be sure to prefix your name with the `@` symbol.

### XMPP (Jabber)

Sends direct messages to your Duke Jabber account.  Enter your NetID, plus the `@duke.edu` suffix.
