# Zabbix User Creation

Top tab: Administration

Sub-tab: Users
  - Create user
    - User form
      - Alias: NetID (unqualified)
      - Name: First name
      - Last Name: Last name
      - Groups: [Developers, etc. or as appropriate]
      - Password/Confirmation: [generate random, e.g. with pwgen, won't be used]
      - Language: English (en_US)
      - (skip other fields on this form, but don't submit yet ...)
    - Permissions form
      - User type: Zabbix Admin (need this to be able to configure own media)
      - Submit: Add
